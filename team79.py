#Team number 79
import sys
import random
import signal
import timeit
from operator import itemgetter

class Player79:
	def __init__(self):
		self.marker = "" #x or o of player
		self.no_of_moves = 0
		self.force_move = [(3,3),(3,5),(5,3),(5,5)] # 4 corners of the centre block
		self.depth_level = 1 #set the depth to which minimax tree will be searched
		self.dia_list = [0,2,3,5,6,8]
		self.centre_list = [1,4,7]
		self.centre_moves = [(1,1),(1,4),(1,7),(4,1),(4,4),(4,7),(7,1),(7,4),(7,7)] # All possible centres of the 9x9
		self.start = 0 
		pass

	def move(self,temp_board,temp_block_stat,old_move,flag):
		"Return Move to be played by Bot"
		self.start = timeit.default_timer() #time at which move started
		
		self.marker = flag
		if self.marker == 'x':
			self.oppmarker = 'o'  #x or o  of opponent
		else :
			self.oppmarker = 'x'

		''' Change the depth level . CUrrently hard coded to increase at fixed number of moves. 
		Improve ply search as game progreses '''
		if self.no_of_moves >= 10 :
			self.depth_level = 4

		if self.no_of_moves >= 18 :
			self.depth_level = 5

		if self.no_of_moves >= 26 :
			self.depth_level = 7 


		print self.no_of_moves
		
		block_stat_prob = [0,0,0,0,0,0,0,0,0] #Initially set to 0. Holds the probability of winning/losing a block at every move

		bl_no = 0
		for choice in self.centre_moves :
			block_stat_prob[bl_no] = self.intra_block_calculation(choice,temp_board)
			bl_no += 1
			
		blocks_allowed = self.determine_blocks_allowed(old_move, temp_block_stat)
		cells = self.get_empty_out_of(temp_board, blocks_allowed,temp_block_stat)

		if self.no_of_moves == 0 and self.marker == 'x' :
			get_move = self.force_move[random.randrange(4)]
		else :
			get_val,get_move = self.minimax(temp_board,temp_block_stat,old_move,0,-99999999,99999999,block_stat_prob,0)

		print self.depth_level
		
		self.no_of_moves += 1
		stop = timeit.default_timer()
		
		print  "movetime " + str(stop - self.start)
		
		return get_move

	def determine_blocks_allowed(self,cur_move, block_stat):
		"Determine the blocks that can be played in depending on opponent move"
		balist = []

		if cur_move[0] % 3 == 0 and cur_move[1] % 3 == 0:
			balist = [1,3]
		elif cur_move[0] % 3 == 0 and cur_move[1] % 3 == 2:
			balist = [1,5]
		elif cur_move[0] % 3 == 2 and cur_move[1] % 3 == 0:
			balist = [3,7]
		elif cur_move[0] % 3 == 2 and cur_move[1] % 3 == 2:
			balist = [5,7]
		elif cur_move[0] % 3 == 1 and cur_move[1] % 3 == 2:
			balist = [2,8]
		elif cur_move[0] % 3 == 1 and cur_move[1] % 3 == 1:
			balist = [4]
		elif cur_move[0] % 3 == 0 and cur_move[1] % 3 == 1:
			balist = [0,2]
		elif cur_move[0] % 3 == 1 and cur_move[1] % 3 == 0:
			balist = [0,6]
		elif cur_move[0] % 3 == 2 and cur_move[1] % 3 == 1:
			balist = [6,8]
		else:
			sys.exit(1)
		
		fab_list = []
		for i in balist:
			if block_stat[i] == '-':
				fab_list.append(i)

		return fab_list

	def get_empty_out_of(self,gameboard, bl_list,block_stat):
		"Return all possible moves for the given blocks that can be played in"
		cells = []  # it will be list of tuples
		#Iterate over possible blocks and get empty cells
		for idb in bl_list:
			idr = idb/3
			idc = idb%3
			for row in range(idr*3,idr*3+3):
				for col in range(idc*3,idc*3+3):
					if gameboard[row][col] == '-':
						cells.append((row,col))

		# If all the possible blocks are full, you can move anywhere
		if cells == []:
			bl_list_new = []
			bl_list_all = [0,1,2,3,4,5,6,7,8]
			for choice in bl_list_all:
				if block_stat[choice]=='-':
					bl_list_new.append(choice)

			for idb in bl_list_new:
				idr = idb/3
				idc = idb%3
				for row in range(idr*3,idr*3+3):
					for col in range(idc*3,idc*3+3):
						if gameboard[row][col] == '-':
							cells.append((row,col))

		return cells

	def CRCalculation(self,pcount,ocount):
		"Values to be used for utility calculation intra block"
		net_value = 0

		if ocount == 0 :
			if pcount == 1 :
				net_value += 1
			elif pcount == 2 :
				net_value += 10
			elif pcount == 3 :
				net_value += 100

		if pcount == 0 :
			if ocount == 1 :
				net_value -= 1
			elif ocount == 2 :
				net_value -= 10
			elif ocount == 3 :
				net_value -= 100

		return net_value

	def final_result(self,bstat_temp):
		"Return the result of the game depending on the virtual move made"
		result = 0
		
		for row in range(0,8,3):
			pcount=0
			ocount=0
			for col in range(row,row+3):
				if bstat_temp[col] == self.marker:
					pcount += 1
				elif bstat_temp[col] == self.oppmarker:
					ocount += 1
			if pcount == 3 :
				result = 1
			elif ocount == 3:
				result = -1
				
		for col in range(0,3):
			pcount=0
			ocount=0
			for row in range(col,8,3):
				if bstat_temp[row] == self.marker:
					pcount += 1
				elif bstat_temp[row] == self.oppmarker:
					ocount += 1
			if pcount == 3 :
				result = 1
			elif ocount == 3:
				result = -1

		ocount=0
		pcount=0
		for block in range(0,9,4):
			if bstat_temp[block] == self.marker:
				pcount += 1
			elif bstat_temp[block] == self.oppmarker:
				ocount += 1
				
		if pcount == 3 :
			result = 1
		elif ocount == 3:
			result = -1

		ocount=0
		pcount=0
		for block in range(2,8,2):
			if bstat_temp[block] == self.marker:
				pcount += 1
			elif bstat_temp[block] == self.oppmarker:
				ocount += 1
				
		if pcount == 3 :
			result = 1
		elif ocount == 3:
			result = -1
			
		return result
			
	def bstat_CRCalculation(self,pcount,ocount,dcount,prob_val):
		"Utility calculation for the blocks (inter block calculation)"
		net_value = 0

		if ocount == 0 and dcount == 0 :
			if pcount == 1 :
				net_value += 200
			elif pcount == 2 :
				net_value += 700
			elif pcount == 3 :
				net_value += 3500

		if pcount == 0 and dcount == 0:
			if ocount == 1 :
				net_value -= 200
			elif ocount == 2 :
				net_value -= 700
			elif ocount == 3 :
				net_value -= 3500

		if net_value != 0 or (dcount==0 and pcount == 0 and ocount==0) :
			net_value += prob_val
			
		return net_value

	def block_prob_calculation(self,block_stat,block_prob):
		"Utility value calculation of a block (intra block calculation)"
		net_value = 0
		for row in range(0,8,3):
			pcount=0
			ocount=0
			dcount=0
			tempval=0
			for col in range(row,row+3):
				if block_stat[col] == self.marker:
					pcount += 1
				elif block_stat[col] == self.oppmarker:
					ocount += 1
				elif block_stat[col] == 'D':
					dcount += 1
				elif block_stat[col] == '-':
					tempval += block_prob[col]

			net_value += self.bstat_CRCalculation(pcount,ocount,dcount,tempval)
				
		for col in range(0,3):
			pcount=0
			ocount=0
			dcount=0
			tempval=0
			for row in range(col,8,3):
				if block_stat[row] == self.marker:
					pcount += 1
				elif block_stat[row] == self.oppmarker:
					ocount += 1
				elif block_stat[row] == 'D':
					dcount += 1
				elif block_stat[row] == '-':
					tempval += block_prob[row]
					
			net_value += self.bstat_CRCalculation(pcount,ocount,dcount,tempval)

		dcount=0
		ocount=0
		pcount=0
		tempval=0
		for block in range(0,9,4):
			if block_stat[block] == self.marker:
				pcount += 1
			elif block_stat[block] == self.oppmarker:
				ocount += 1
			elif block_stat[block] == 'D':
				dcount += 1
			elif block_stat[block] == '-':
				tempval += block_prob[block]
				
		net_value += self.bstat_CRCalculation(pcount,ocount,dcount,tempval)

		ocount=0
		pcount=0
		dcount=0
		tempval=0
		for block in range(2,8,2):
			if block_stat[block] == self.marker:
				pcount += 1
			elif block_stat[block] == self.oppmarker:
				ocount += 1
			elif block_stat[block] == 'D':
				dcount += 1
			elif block_stat[block] == '-':
				tempval += block_prob[block]
				
		net_value += self.bstat_CRCalculation(pcount,ocount,dcount,tempval)
							
		return net_value

	def get_block(self,cell_move):
		"return block in which move belongs"
		row_inc = (cell_move[0] // 3)*3
		col_inc = cell_move[1] // 3

		return row_inc + col_inc
			
	def cell_preference(self,filtered_list) :
		"In case multiple moves with same utility value present, chose diagonal over side over centre"
		for cell_move in filtered_list :
			if cell_move[1][0] in self.dia_list and cell_move[1][1] in self.dia_list :
				return cell_move #diagonal move

		for cell_move in filtered_list :
			if cell_move[1][0] in self.centre_list and cell_move[1][1] in self.centre_list :
				continue
			else :
				return cell_move #side move
			
		return filtered_list[0]

	def cell_opp_preference(self,filtered_list) :
		for cell_move in filtered_list :
			if cell_move[1][0] in self.centre_list and cell_move[1][1] in self.centre_list :
				return cell_move
			
		for cell_move in filtered_list :
			if cell_move[1][0] in self.dia_list and cell_move[1][1] in self.dia_list :
				return cell_move #diagonal move
			else :
				return cell_move #side move
			
		return filtered_list[0]
	
	def intra_block_calculation(self,cur_move,temp_board):
		'''Calculate utility value of a block
		Utility of a block is calculated by going through the rows columns and diagonals of the block
		values are assigned to them depending on the state and net value is returned'''
		net_value = 0
		if 0<= cur_move[0] <=2 :
			x=0
		elif 3<= cur_move[0] <=5 :
			x=3
		elif 6<= cur_move[0] <=8 :
			x=6

		if 0<= cur_move[1] <=2 :
			y=0
		elif 3<= cur_move[1] <=5 :
			y=3
		elif 6<= cur_move[1] <=8 :
			y=6

		for row in range(x,x+3):
			pcount=0
			ocount=0
			for col in range(y,y+3):
				if temp_board[row][col] == self.marker :
					pcount+=1
				elif temp_board[row][col] == self.oppmarker :
					ocount+=1
			net_value += self.CRCalculation(pcount,ocount)

		for col in range(y,y+3):
			pcount=0
			ocount=0
			for row in range(x,x+3):
				if temp_board[row][col] == self.marker :
					pcount+=1
				elif temp_board[row][col] == self.oppmarker :
					ocount+=1
			net_value += self.CRCalculation(pcount,ocount)

		pcount=0
		ocount=0
		for row,col in zip(range(x,x+3),range(y,y+3)):
			if temp_board[row][col] == self.marker :
				pcount+=1
			elif temp_board[row][col] == self.oppmarker :
				ocount+=1
		net_value += self.CRCalculation(pcount,ocount)

		pcount=0
		ocount=0
		for col,row in zip(range(y+2,y-1,-1),range(x,x+3)):
			if temp_board[row][col] == self.marker :
				pcount+=1
			elif temp_board[row][col] == self.oppmarker :
				ocount+=1
		net_value += self.CRCalculation(pcount,ocount)

		return net_value

	def update_block_prob(self,cell_move,bprob_list,temp_board) :
		"Update the probability of each block "
		block_no = self.get_block(cell_move)
		temp_list = bprob_list[:]
		temp_list[block_no] = self.intra_block_calculation(cell_move,temp_board)
		return temp_list
	
	def utility_value(self,cur_move,temp_board,temp_block_stat,depth,bprob_list):
		"Calculates the net utility value of a node (Board state)"
		net_value = 0

		temp_list = self.update_block_prob(cur_move,bprob_list,temp_board) #Calculates probability of winning certain blocks
		net_value = self.block_prob_calculation(temp_block_stat,temp_list)	
				
		return net_value
	
	def minimax(self,temp_board,temp_block_stat,cur_move,depth,alpha,beta,block_stat_prob,time_flag) :
		"Minimax recursion function (DFS search)"
		
		if depth <= self.depth_level and time_flag==0 :
			blocks_allowed = self.determine_blocks_allowed(cur_move, temp_block_stat)
			result = self.final_result(temp_block_stat)
			cells = []
			
			if result != -1 and result != 1 :
				cells = self.get_empty_out_of(temp_board, blocks_allowed,temp_block_stat)
				
			temp_block_stat_prob = self.update_block_prob(cur_move,block_stat_prob,temp_board)
			
			stop = timeit.default_timer()
			if stop-self.start > 11 : # Check to see if exceeded time (currently 12s per move)
				print "RUNNING OUT OF TIME RETURN"
				time_flag = 1
				
			child_ret_list = [] #list to store all the children nodes (stored as tuple )
			for choice in cells :

				#Make virtual move
				if depth%2 == 0 : 
					bblock_flag,bblock_pos = self.update_lists(temp_board, temp_block_stat, choice, self.marker) #even my move
				else :
					bblock_flag,bblock_pos = self.update_lists(temp_board, temp_block_stat, choice, self.oppmarker) #odd opponent move

				val,best_move = self.minimax(temp_board,temp_block_stat,choice,depth + 1,alpha,beta,temp_block_stat_prob,time_flag) #recursion
							
				child_ret_list.append((val,choice)) #val is value of node, choice is the best move from children

				if val>alpha and depth%2==0 :
					alpha = val
				elif val<beta and depth%2==1 :
					beta = val
					
				#Reverting temporary move made
				temp_board[choice[0]][choice[1]] = '-'
				if(bblock_flag == 1):
					temp_block_stat[bblock_pos] = '-'

				if alpha > beta : #alpha-beta pruning
					break
					
			if len(cells) == 0 : #some end state (win,loss,draw)
				tval = self.utility_value(cur_move,temp_board,temp_block_stat,depth,block_stat_prob)
				if result == 1 : # if state win increase utility 
					tval += 1200 * (10//depth)
				elif result == -1:
					tval -= 1200 * (10//depth)
				else :
					print "Heading for draw"
					
				return (tval,cur_move) #Calculate utility if reached depth
			elif depth%2 == 0 :
				checkval = max(child_ret_list,key=itemgetter(0))
				templist = []

				#Get all moves with same utility values
				for option in child_ret_list :
					if checkval[0] == option[0] :
						templist.append(option)
						
				if depth==0 :
					print templist

				if len(templist) > 1 :
					return self.cell_preference(templist) #Get a better moves of the remaining set of equal moves
				
				return max(child_ret_list,key=itemgetter(0))
			else :
				checkval = min(child_ret_list,key=itemgetter(0))
				templist = []
				for option in child_ret_list :
					if checkval[0] == option[0] :
						templist.append(option)
						
				if len(templist) > 1 :
					return self.cell_opp_preference(templist)
				
				return min(child_ret_list,key=itemgetter(0))
		else : #leaf node
			return (self.utility_value(cur_move,temp_board,temp_block_stat,depth,block_stat_prob),cur_move)
		
	def update_lists(self,game_board, block_stat, move_ret, fl):
		"Perform move on board"
		game_board[move_ret[0]][move_ret[1]] = fl

		block_no = (move_ret[0]/3)*3 + move_ret[1]/3	
		id1 = block_no/3
		id2 = block_no%3
		mflg = 0

		flag = 0
		for i in range(id1*3,id1*3+3):
			for j in range(id2*3,id2*3+3):
				if game_board[i][j] == '-':
					flag = 1

		if block_stat[block_no] == '-':
			if game_board[id1*3][id2*3] == game_board[id1*3+1][id2*3+1] and game_board[id1*3+1][id2*3+1] == game_board[id1*3+2][id2*3+2] and game_board[id1*3+1][id2*3+1] != '-' and game_board[id1*3+1][id2*3+1] != 'D':
				mflg=1
			if game_board[id1*3+2][id2*3] == game_board[id1*3+1][id2*3+1] and game_board[id1*3+1][id2*3+1] == game_board[id1*3][id2*3 + 2] and game_board[id1*3+1][id2*3+1] != '-' and game_board[id1*3+1][id2*3+1] != 'D':
				mflg=1
			if mflg != 1:
						for i in range(id2*3,id2*3+3):
							if game_board[id1*3][i]==game_board[id1*3+1][i] and game_board[id1*3+1][i] == game_board[id1*3+2][i] and game_board[id1*3][i] != '-' and game_board[id1*3][i] != 'D':
									mflg = 1
									break
			if mflg != 1:
						for i in range(id1*3,id1*3+3):
							if game_board[i][id2*3]==game_board[i][id2*3+1] and game_board[i][id2*3+1] == game_board[i][id2*3+2] and game_board[i][id2*3] != '-' and game_board[i][id2*3] != 'D':
									mflg = 1
									break

		if mflg == 1:
			block_stat[block_no] = fl
			
		if flag == 0 and mflg == 0:
			block_stat[block_no] = 'D'
			mflg = 1
		
		return mflg , block_no
